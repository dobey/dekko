<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja">
<context>
    <name>AccountSettingsList</name>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="26"/>
        <source>Account Settings</source>
        <translation>アカウントの設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="56"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="60"/>
        <source>Incoming Server</source>
        <translation>受信サーバー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="64"/>
        <source>Outgoing Server</source>
        <translation>送信サーバー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="68"/>
        <source>Copies and Folders</source>
        <translation>コピーとフォルダー</translation>
    </message>
</context>
<context>
    <name>AddAnotherUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="30"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="89"/>
        <source>New account created.</source>
        <translation>新しいアカウントが作成されました。</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="98"/>
        <source>Continue</source>
        <translation>続ける</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="113"/>
        <source>Add another</source>
        <translation>他のものを追加する</translation>
    </message>
</context>
<context>
    <name>AddressBookList</name>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="12"/>
        <source>Addressbooks</source>
        <translation>連絡先</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="65"/>
        <source>Add Collection</source>
        <translation>コレクションを追加</translation>
    </message>
</context>
<context>
    <name>AddressBookStage</name>
    <message>
        <location filename="../plugins/core/contacts/AddressBookStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation>お待ち下さい</translation>
    </message>
</context>
<context>
    <name>AttachmentPanel</name>
    <message>
        <location filename="../plugins/core/mail/components/AttachmentPanel.qml" line="81"/>
        <source>Attachments</source>
        <translation>添付ファイル</translation>
    </message>
</context>
<context>
    <name>AttachmentPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/AttachmentPopover.qml" line="46"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>AttachmentsStage</name>
    <message>
        <location filename="../plugins/extensions/attachments/AttachmentsStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation>お待ち下さい</translation>
    </message>
</context>
<context>
    <name>AuthenticationSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="45"/>
        <source>Authentication</source>
        <translation>認証</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="60"/>
        <source>PLAIN</source>
        <translation>平文</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="61"/>
        <source>LOGIN</source>
        <translation>ログイン</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="62"/>
        <source>CRAM-MD5</source>
        <translation>CRAM-MD5</translation>
    </message>
</context>
<context>
    <name>AutoConfigState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="35"/>
        <source>Searching for configuration.</source>
        <translation>設定を検索しています。</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="77"/>
        <source>IMAP server found</source>
        <translation>IMAPサーバーが見つかりました</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="78"/>
        <source>A IMAP server configuration was found for you domain.

Would you like to use this instead?</source>
        <translation>あなたのドメインに適したIMAPの設定があります。

これに設定しますか?</translation>
    </message>
</context>
<context>
    <name>BottomEdgeComposer</name>
    <message>
        <location filename="../plugins/core/mail/composer/BottomEdgeComposer.qml" line="65"/>
        <source>Attachments</source>
        <translation>添付ファイル</translation>
    </message>
</context>
<context>
    <name>CalendarStage</name>
    <message>
        <location filename="../plugins/core/calendar/CalendarStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation>お待ち下さい</translation>
    </message>
</context>
<context>
    <name>ComposeWindow</name>
    <message>
        <location filename="../plugins/core/mail/composer/ComposeWindow.qml" line="22"/>
        <source>Dekko Composer</source>
        <translation>Dekko コンポーザー</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../plugins/core/mail/composer/Composer.qml" line="52"/>
        <source>Attach</source>
        <translation>添付</translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="50"/>
        <location filename="../plugins/core/mail/dialogs/ConfirmationDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation>取り消し</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="63"/>
        <location filename="../plugins/core/mail/dialogs/ConfirmationDialog.qml" line="63"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>ContactFilterView</name>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="112"/>
        <source>Add contact</source>
        <translation>連絡先を追加</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="123"/>
        <source>Send message</source>
        <translation>メッセージを送信</translation>
    </message>
</context>
<context>
    <name>ContactListPage</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactListPage.qml" line="11"/>
        <source>Address book</source>
        <translation>連絡先</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="11"/>
        <source>Contact</source>
        <translation>連絡先</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="61"/>
        <source>Email</source>
        <translation>電子メール</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="78"/>
        <source>Phone</source>
        <translation>電話番号</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="92"/>
        <source>Address</source>
        <translation>住所</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="96"/>
        <source>Street</source>
        <translation>番地</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="102"/>
        <source>City</source>
        <translation>市町村</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="108"/>
        <source>Zip</source>
        <translation>郵便番号</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="114"/>
        <source>Country</source>
        <translation>国</translation>
    </message>
</context>
<context>
    <name>ContactsListView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactsListView.qml" line="36"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
<context>
    <name>ContentBlockedNotice</name>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="41"/>
        <source>Remote content blocked</source>
        <translation>リモートコンテンツがブロックされています</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="54"/>
        <source>Allow</source>
        <translation>許可</translation>
    </message>
</context>
<context>
    <name>ContributorsPage</name>
    <message>
        <location filename="../plugins/core/mail/views/ContributorsPage.qml" line="25"/>
        <source>Contributors</source>
        <translation>コントリビューター</translation>
    </message>
</context>
<context>
    <name>CopyFoldersGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="30"/>
        <source>Copies and Folders</source>
        <translation>コピーとフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="124"/>
        <source>Standard folders</source>
        <translation>標準フォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="136"/>
        <source>Detect standard folders</source>
        <translation>標準フォルダーを検出</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="152"/>
        <source>Detect</source>
        <translation>検出</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="173"/>
        <source>Base folder</source>
        <translation>親フォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="175"/>
        <source>Leave empty if you are unsure</source>
        <translation>不明な場合、空白のままにしておいてください</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="190"/>
        <source>Inbox folder</source>
        <translation>受信トレイフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="205"/>
        <source>Drafts folder</source>
        <translation>下書きフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="220"/>
        <source>Spam folder</source>
        <translation>スパムフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="235"/>
        <source>Sent folder</source>
        <translation>送信済みフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="250"/>
        <source>Outbox folder</source>
        <translation>送信トレイフォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="265"/>
        <source>Trash folder</source>
        <translation>ゴミ箱フォルダー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="274"/>
        <source>Sending messages</source>
        <translation>メッセージを送信中</translation>
    </message>
</context>
<context>
    <name>DefaultMessagePage</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="182"/>
        <source>From:</source>
        <translation>From:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="235"/>
        <source>To:</source>
        <translation>To:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="240"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DefaultPlugin</name>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="11"/>
        <source>Internal</source>
        <translation>内部</translation>
    </message>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="106"/>
        <source>Default</source>
        <translation>デフォルト</translation>
    </message>
</context>
<context>
    <name>DekkoHeader</name>
    <message>
        <location filename="../imports/components/DekkoHeader.qml" line="204"/>
        <source>Enter search...</source>
        <translation>検索ワード…</translation>
    </message>
</context>
<context>
    <name>DekkoWebView</name>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="142"/>
        <source>Open in browser?</source>
        <translation>ブラウザで開きますか？</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="143"/>
        <source>Confirm to open %1 in web browser</source>
        <translation>%1 をブラウザで開きますか</translation>
    </message>
</context>
<context>
    <name>DetailList</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="47"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="52"/>
        <source>To:</source>
        <translation>To:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="58"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DetailsGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="27"/>
        <source>Details</source>
        <translation>詳細</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="62"/>
        <source>Account name</source>
        <translation>アカウント名</translation>
    </message>
</context>
<context>
    <name>DisplaySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="28"/>
        <source>Navigation menu</source>
        <translation>ナビゲーションメニュー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="32"/>
        <source>Show smart folders</source>
        <translation>スマートフォルダを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="42"/>
        <source>Show favourite folders</source>
        <translation>お気に入りフォルダを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="53"/>
        <source>Messages</source>
        <translation>メッセージ</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="57"/>
        <source>Show avatars</source>
        <translation>アバターを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="67"/>
        <source>Prefer plain text</source>
        <translation>プレーンテキストを使用</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPage.qml" line="21"/>
        <source>Display Settings</source>
        <translation>表示設定</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPopup.qml" line="21"/>
        <source>Display Settings</source>
        <translation>表示設定</translation>
    </message>
</context>
<context>
    <name>EncryptionSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="46"/>
        <source>Encryption</source>
        <translation>暗号化</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="61"/>
        <source>No encryption</source>
        <translation>暗号化なし</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="62"/>
        <source>Use encryption (STARTTLS)</source>
        <translation>暗号化 (STARTTLS)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="63"/>
        <source>Force encryption (SSL/TLS)</source>
        <translation>暗号化を強制 (SSL/TLS)</translation>
    </message>
</context>
<context>
    <name>ExpandablePanel</name>
    <message>
        <location filename="../imports/components/ExpandablePanel.qml" line="62"/>
        <source>Attachments</source>
        <translation>添付ファイル</translation>
    </message>
</context>
<context>
    <name>FilePickerDialog</name>
    <message>
        <location filename="../imports/dialogs/FilePickerDialog.qml" line="23"/>
        <location filename="../plugins/core/mail/dialogs/FilePickerDialog.qml" line="23"/>
        <source>Add Attachment</source>
        <translation>添付ファイルを追加</translation>
    </message>
</context>
<context>
    <name>FolderListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Un-favourite</source>
        <translation>お気に入り解除</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Favourite</source>
        <translation>お気に入り</translation>
    </message>
</context>
<context>
    <name>HtmlViewer</name>
    <message>
        <location filename="../plugins/extensions/html-viewer/HtmlViewer.qml" line="11"/>
        <source>HTML Viewer</source>
        <translation>HTML ビューア</translation>
    </message>
</context>
<context>
    <name>IdentitiesListPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="14"/>
        <source>Identities</source>
        <translation>氏名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="106"/>
        <source> (Default)</source>
        <translation> （デフォルト）</translation>
    </message>
</context>
<context>
    <name>IdentityInput</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="54"/>
        <source>Default identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="67"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="105"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="110"/>
        <source>Email Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="115"/>
        <source>Reply-To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="119"/>
        <source>Signature</source>
        <translation>署名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="137"/>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="145"/>
        <source>New identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="169"/>
        <source>Edit identity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IncomingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="28"/>
        <source>Incoming Server</source>
        <translation>受信サーバー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="105"/>
        <source>Hostname</source>
        <translation>ホスト名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="114"/>
        <source>Port</source>
        <translation>ポート</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="123"/>
        <source>Username</source>
        <translation>ユザーネーム</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="132"/>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="135"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="142"/>
        <source>Show password</source>
        <translation>パスワードを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="148"/>
        <source>Security settings</source>
        <translation>セキュリティー設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="183"/>
        <source>Allow untrusted certificates</source>
        <translation>信頼できない証明書を許可</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="188"/>
        <source>Server settings</source>
        <translation>サーバー設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="193"/>
        <source>Check for new mail on start</source>
        <translation>起動時に新着メールを確認する</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="199"/>
        <source>Enable IMAP IDLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="206"/>
        <source>Check interval (minutes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="213"/>
        <source>Check when roaming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="219"/>
        <source>Maximum mail size (MB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="225"/>
        <source>No maximum mail size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="233"/>
        <source>Automatically download attachments</source>
        <translation>添付ファイルを自動的にダウンロード</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="239"/>
        <source>Allowed to delete mail</source>
        <translation>メールの削除を許可</translation>
    </message>
</context>
<context>
    <name>LicensesPage</name>
    <message>
        <location filename="../plugins/core/mail/views/LicensesPage.qml" line="25"/>
        <source>Licenses</source>
        <translation>ライセンス</translation>
    </message>
</context>
<context>
    <name>MailSettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="11"/>
        <source>Mail Settings</source>
        <translation>メール設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="24"/>
        <source>Accounts</source>
        <translation>アカウント</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="34"/>
        <source>Identities</source>
        <translation>氏名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="44"/>
        <source>Display</source>
        <translation>ディスプレー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="51"/>
        <source>Privacy</source>
        <translation>プライバシー</translation>
    </message>
</context>
<context>
    <name>MailSettingsAction</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettingsAction.qml" line="7"/>
        <source>Mail</source>
        <translation>メール</translation>
    </message>
</context>
<context>
    <name>MailUtils</name>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="27"/>
        <source>To</source>
        <translation>宛先</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="29"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="31"/>
        <source>Bcc</source>
        <translation>Bcc</translation>
    </message>
</context>
<context>
    <name>MailboxPickerPage</name>
    <message>
        <location filename="../plugins/core/mail/views/MailboxPickerPage.qml" line="33"/>
        <source>Select folder</source>
        <translation>フォルダーを選択</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="../qml/MainUI.qml" line="16"/>
        <source>Dekko Mail</source>
        <translation>Dekko Mail</translation>
    </message>
</context>
<context>
    <name>ManageAccountsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/ManageAccountsPage.qml" line="26"/>
        <source>Manage accounts</source>
        <translation>アカウントの管理</translation>
    </message>
</context>
<context>
    <name>ManualInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="28"/>
        <source>Server configuration</source>
        <translation>サーバー構成</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="33"/>
        <source>IMAP Server:</source>
        <translation>IMAPサーバー:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="46"/>
        <source>POP3 Server:</source>
        <translation>POP3サーバー:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="59"/>
        <source>SMTP Server:</source>
        <translation>SMTPサーバー:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="71"/>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="75"/>
        <source>Next</source>
        <translation>次へ</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Password empty</source>
        <translation>パスワードが空です</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Would you like to continue?</source>
        <translation>続行しますか？</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../plugins/extensions/Markdown/MarkdownEditor.qml" line="66"/>
        <source>Preview</source>
        <translation>プレビュー</translation>
    </message>
</context>
<context>
    <name>MessageActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="44"/>
        <source>Reply all</source>
        <translation>全員に返信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="52"/>
        <source>Forward</source>
        <translation>進む</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="65"/>
        <source>Move</source>
        <translation>移動</translation>
    </message>
</context>
<context>
    <name>MessageHeader</name>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>Hide details</source>
        <translation>詳細を隠す</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>View details</source>
        <translation>詳細を表示</translation>
    </message>
</context>
<context>
    <name>MessageListActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as unread</source>
        <translation type="unfinished">未読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as read</source>
        <translation type="unfinished">既読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as not important</source>
        <translation type="unfinished">重要でないとマーク</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as important</source>
        <translation type="unfinished">重要とマーク</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="60"/>
        <source>Mark as spam</source>
        <translation type="unfinished">スパムとしてマーク</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="73"/>
        <source>To-do</source>
        <translation type="unfinished">やること</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="89"/>
        <source>Done</source>
        <translation>完了</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="103"/>
        <source>Reply</source>
        <translation type="unfinished">返信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="111"/>
        <source>Reply all</source>
        <translation type="unfinished">全員に返信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="119"/>
        <source>Forward</source>
        <translation>進む</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="133"/>
        <source>Move</source>
        <translation>移動</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="142"/>
        <source>Restore to %1</source>
        <translation type="unfinished">戻す</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="151"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>MessageListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="41"/>
        <source>Un-mark flagged</source>
        <translation type="unfinished">マークを外しました</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="41"/>
        <source>Mark flagged</source>
        <translation type="unfinished">マークしました</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Mark as un-read</source>
        <translation type="unfinished">未読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Mark as read</source>
        <translation type="unfinished">既読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="57"/>
        <source>Move message</source>
        <translation type="unfinished">メッセージを移動</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="62"/>
        <source>Context menu</source>
        <translation>コンテキストメニュー</translation>
    </message>
</context>
<context>
    <name>MessageListView</name>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="106"/>
        <source>Unselect all</source>
        <translation>すべて選択解除</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="106"/>
        <source>Select all</source>
        <translation>すべて選択</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="118"/>
        <source>Star</source>
        <translation type="unfinished">スター</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="118"/>
        <source>Remove star</source>
        <translation type="unfinished">スターを削除</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="122"/>
        <source>Mark as un-read</source>
        <translation>未読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="122"/>
        <source>Mark as read</source>
        <translation>既読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="129"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="312"/>
        <source>Load more messages ...</source>
        <translation type="unfinished">さらにメッセージを読み込む...</translation>
    </message>
</context>
<context>
    <name>MessageViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="46"/>
        <source>Open in browser</source>
        <translation>ブラウザで開く</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="54"/>
        <source>Copy link</source>
        <translation>リンクをコピー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="63"/>
        <source>Share link</source>
        <translation type="unfinished">共有リンク</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="77"/>
        <source>Reply</source>
        <translation>返信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="86"/>
        <source>Reply all</source>
        <translation>全員に返信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="94"/>
        <source>Forward</source>
        <translation>進む</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="106"/>
        <source>View source</source>
        <translation type="unfinished">ソースを見る</translation>
    </message>
</context>
<context>
    <name>NavMenuAccountSettingsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="29"/>
        <source>Manage accounts</source>
        <translation>アカウントを管理</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="45"/>
        <source>Display settings</source>
        <translation>ディスプレイの設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="67"/>
        <source>Privacy settings</source>
        <translation>プライバシーの設定</translation>
    </message>
</context>
<context>
    <name>NavMenuContactsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="27"/>
        <source>Addressbook</source>
        <translation>アドレス帳</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="43"/>
        <source>Recent contacts</source>
        <translation type="unfinished">最近の連絡先</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="58"/>
        <source>Import contacts</source>
        <translation>連絡先をインポート</translation>
    </message>
</context>
<context>
    <name>NavMenuDekkoVisualModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="27"/>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="45"/>
        <source>Licenses</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="63"/>
        <source>Contributors</source>
        <translation type="unfinished">寄付者</translation>
    </message>
</context>
<context>
    <name>NavMenuModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="98"/>
        <source>Smart folders</source>
        <translation>スマートフォルダ</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="120"/>
        <source>Folders</source>
        <translation>フォルダ</translation>
    </message>
</context>
<context>
    <name>NavMenuPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="30"/>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="64"/>
        <source>Mail</source>
        <translation>メール</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="67"/>
        <source>Contacts</source>
        <translation>コンタクト</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="70"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="73"/>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
</context>
<context>
    <name>NavMenuStandardFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/NavMenuStandardFolderDelegate.qml" line="178"/>
        <source>Inbox (%1)</source>
        <translation>受信トレイ  (％1)</translation>
    </message>
</context>
<context>
    <name>NavSideBar</name>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="155"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="155"/>
        <source>Smart folders</source>
        <translation>スマートフォルダ</translation>
    </message>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="169"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="169"/>
        <source>Folders</source>
        <translation>フォルダ</translation>
    </message>
</context>
<context>
    <name>NavViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="44"/>
        <source>Sync folder</source>
        <translation>同期フォルダ</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="53"/>
        <source>Send pending</source>
        <translation type="unfinished">保留中の送信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="70"/>
        <source>Mark folder read</source>
        <translation>フォルダを既読にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="79"/>
        <source>Mark all done</source>
        <translation type="unfinished">すべてに完了をマーク</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="101"/>
        <source>Empty trash</source>
        <translation>ゴミ箱を空にする</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="110"/>
        <source>Folder properties</source>
        <translation>フォルダのプロパティ</translation>
    </message>
</context>
<context>
    <name>NewAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NewAccountsUI.qml" line="32"/>
        <source>New account</source>
        <translation>新しいアカウント</translation>
    </message>
</context>
<context>
    <name>NoAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="30"/>
        <source>Accounts</source>
        <translation>アカウント</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="92"/>
        <source>No email account is setup.</source>
        <translation>電子メールアカウントが設定されていません。</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="101"/>
        <source>Add now</source>
        <translation>今すぐ追加</translation>
    </message>
</context>
<context>
    <name>NotesStage</name>
    <message>
        <location filename="../plugins/core/notes/NotesStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished">お待ちください</translation>
    </message>
</context>
<context>
    <name>NothingSelectedPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NothingSelectedPage.qml" line="55"/>
        <source>No message selected</source>
        <translation type="unfinished">メッセージが選択されていません</translation>
    </message>
</context>
<context>
    <name>OutgoingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="28"/>
        <source>Outgoing Server</source>
        <translation>送信サーバー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="75"/>
        <source>Hostname</source>
        <translation>ホスト名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="83"/>
        <source>Port</source>
        <translation>ポート</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="92"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="100"/>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="103"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="110"/>
        <source>Show password</source>
        <translation>パスワードを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="116"/>
        <source>Security settings</source>
        <translation>セキュリティ設定</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="149"/>
        <source>Authenticate from server capabilities</source>
        <translation type="unfinished">サーバー機能から認証する</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="155"/>
        <source>Allow untrusted certificates</source>
        <translation>信頼できない証明書を許可</translation>
    </message>
</context>
<context>
    <name>PrivacySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="28"/>
        <source>Message content</source>
        <translation>メッセージ内容</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="32"/>
        <source>Allow remote content</source>
        <translation>リモートコンテンツを許可</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="42"/>
        <source>Auto load images</source>
        <translation type="unfinished">自動で画像をロード</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPage.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>プライバシー設定</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPopup.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>プライバシー設定</translation>
    </message>
</context>
<context>
    <name>RecipientField</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientField.qml" line="97"/>
        <source>Enter an address</source>
        <translation>アドレスを入力</translation>
    </message>
</context>
<context>
    <name>RecipientInfo</name>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="41"/>
        <source>Back</source>
        <translation>戻る</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="93"/>
        <source>Copy to clipboard</source>
        <translation>クリップボードにコピー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="107"/>
        <source>Add to addressbook</source>
        <translation>アドレスブックに追加</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="122"/>
        <source>Send message</source>
        <translation>メッセージを送信</translation>
    </message>
</context>
<context>
    <name>RecipientInputContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="55"/>
        <source>Add CC</source>
        <translation>CCを追加</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="68"/>
        <source>Add BCC</source>
        <translation>BCCを追加</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="80"/>
        <source>Add contact</source>
        <translation>連絡先を追加</translation>
    </message>
</context>
<context>
    <name>RecipientPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="80"/>
        <source>Copy to clipboard</source>
        <translation>クリップボードにコピー</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="89"/>
        <source>Add to addressbook</source>
        <translation>アドレスブックに追加</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="98"/>
        <source>Send message</source>
        <translation>メッセージを送信</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="108"/>
        <source>Remove</source>
        <translation>削除</translation>
    </message>
</context>
<context>
    <name>SenderIdentityField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SenderIdentityField.qml" line="53"/>
        <source>From:</source>
        <translation>差出人</translation>
    </message>
</context>
<context>
    <name>ServerDetails</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="69"/>
        <source>Hostname</source>
        <translation>ホスト名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="78"/>
        <source>Port</source>
        <translation>ポート</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="100"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="109"/>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="112"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="119"/>
        <source>Show password</source>
        <translation>パスワードを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="140"/>
        <source>Allow untrusted certificates</source>
        <translation>信頼できない証明書を許可</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../plugins/core/settings/Settings.qml" line="11"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../plugins/core/settings/SettingsWindow.qml" line="26"/>
        <source>Dekko Settings</source>
        <translation>Dekko設定</translation>
    </message>
</context>
<context>
    <name>SetupWizardWindow</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/SetupWizardWindow.qml" line="20"/>
        <source>Mail Setup Wizard</source>
        <translation>メール設定ウィザード</translation>
    </message>
</context>
<context>
    <name>SmartFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/SmartFolderDelegate.qml" line="155"/>
        <source>Inbox (%1)</source>
        <translation>受信トレイ (%1)</translation>
    </message>
</context>
<context>
    <name>SubjectField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SubjectField.qml" line="56"/>
        <source>Subject:</source>
        <translation>件名：</translation>
    </message>
</context>
<context>
    <name>SyncState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/SyncState.qml" line="35"/>
        <source>Synchronizing account.</source>
        <translation>同期アカウント</translation>
    </message>
</context>
<context>
    <name>TitledTextField</name>
    <message>
        <location filename="../imports/components/TitledTextField.qml" line="61"/>
        <source> (Required)</source>
        <translation> (必須)</translation>
    </message>
</context>
<context>
    <name>UserInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="35"/>
        <source>Name</source>
        <translation>名前</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="36"/>
        <source>Full name</source>
        <translation>フルネーム</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="43"/>
        <source>Description</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="44"/>
        <source>E.g Home, Work...</source>
        <translation>家、仕事...</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="51"/>
        <source>Email address</source>
        <translation>Emailメールアドレス</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="53"/>
        <source>email@example.org</source>
        <translation>email@example.org</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="59"/>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="62"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="68"/>
        <source>Show password</source>
        <translation>パスワードを表示</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="78"/>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="82"/>
        <source>Next</source>
        <translation>次え</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Password empty</source>
        <translation>パスワードが空です</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Would you like to continue?</source>
        <translation>続行しますか？</translation>
    </message>
</context>
<context>
    <name>ValidationState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/ValidationState.qml" line="36"/>
        <source>Validating credentials.</source>
        <translation type="unfinished">資格情報を検証しています。</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="25"/>
        <location filename="../plugins/core/mail/dialogs/VersionDialog.qml" line="25"/>
        <source>Version</source>
        <translation>バージョン</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="29"/>
        <location filename="../plugins/core/mail/dialogs/VersionDialog.qml" line="29"/>
        <source>Close</source>
        <translation>閉じる</translation>
    </message>
</context>
</TS>
